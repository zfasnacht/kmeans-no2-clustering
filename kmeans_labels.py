from sklearn.cluster import KMeans
import h5py
import numpy as np
import pickle 

class kmeans_cluster:
    def  __init__(self, inputs,n_cluster=10):
        self.inputs = inputs
        self.n_cluster = n_cluster
    #Fitting inputs to kmeans clustering
    #https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    def fit(self):
        kmeans = KMeans(init='k-means++',n_clusters=self.n_cluster)
        kmeans.fit(self.inputs)
        self.kmeans = kmeans
        self.labels = self.kmeans.labels_

    #Saving Kmeans function to pickle file so that it can be read in and used in other codes 
    def save(self,filename):
        with open(filename,'wb') as f:
            pickle.dump(self.kmeans,f)

    #Read in kmeans function from pickle file
    def read(self,filename):
        with open(filename,'wb') as f:
            self.kmeans = pickle.load(f)

    #Use this function to predict labels when using kmeans on new input data
    def predict(self,inputs):
        self.labels = kmeans.predict(inputs)

#Reading all data in from hdf5 file and storing in a python dictionary
def read_h5(filename):
    data = {}
    f = h5py.File(filename,'r')
    for key in f.keys():
        data[key] = f[key][:]
    return data

if __name__ == "__main__":

    #Reading sample NO2 file
    no2_data = read_h5('Model_NNInputs_2020_Pixel_Basin.h5')
    
    #Averaging NO2 data over the year so that cities can be clustered based on mean inputs
    for key in no2_data.keys():
        if no2_data[key].ndim==2:
            no2_data[key] = np.nanmean(no2_data[key],axis=-1)

    
    cities = no2_data['City']

    #Stacking desired cluster inputs. This is just a sample, can use any desired inputs for clustering
    cluster_inp = np.swapaxes(np.vstack((no2_data['T2M'],no2_data['GMI_NO2'],no2_data['POPCOUNT'])),0,1)
    
    #Performing Kmeans clustering on inputs, set up now for kmenas to use 10 clusters
    n_cluster = 10
    cluster = kmeans_cluster(cluster_inp,n_cluster=n_cluster)
    cluster.fit()
    
    #Printing out the clusters and their corresponding cities 
    for i in range(n_cluster):
        inds, = np.where(cluster.labels == i)
        print('Cluster '+str(i)+': '+','.join(cities[inds]))

